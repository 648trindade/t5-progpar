/**
 * Programação Paralela - ELC 139
 * Profa. Andrea Schwertner Charão
 * Aluno docência Alberto Francisco Kummer Neto
 *
 * Aplicação "inversor"
 * Descrição: Troca a direção e sentido de dois vetores e faz comparações.
 * Resultado esperado: [1 0 1 1]
 */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sys/time.h>

#define TOL 0.0000005

#define EQ(A,B) ((abs(A - B) <= TOL))

void invert(double *vec, int sz) {
   int i;
   #pragma omp parallel for private(i) shared(vec)
   for(i = 0; i < sz; ++i)
      vec[i] *= -1.0;
}

long wtime()
{
   struct timeval t;
   gettimeofday(&t, NULL);
   return t.tv_sec*1000000 + t.tv_usec;
}

void initVector(double *vec, int sz) {
   int i;
   double vi = 0.5;
   for (i = 0; i < sz; ++i) {
      if (i%3 == 0 || i%11 == 0 || i%57 == 0)
         vi *= -1.0;
      vec[i] = vi;
   }
}

int equals(double *v1, double *v2, int sz) {
   int i;
   for (i = 0; i < sz; ++i)
      if (!EQ(v1[i], v2[i]))
         return 0;
   return 1;
}

int main(int argc, char **argv) {
   const int N = 1000000;
   long start = wtime();
   double *a = (double*) malloc(sizeof(double) * N);
   double b[N];

   int results[4] = {-1,-1,-1,-1};

   #pragma omp parallel sections
   {
       #pragma omp section
       initVector(a, N);
       #pragma omp section
       initVector(b, N);
   }

   results[0] = equals(a, b, N); // Sem alterar os valores de 'a' e 'b';
   invert(a, N);
   results[1] = equals(a, b, N); // Somente 'a' invertido.
   invert(b, N);
   results[2] = equals(a, b, N); // 'a' e 'b' invertido.
   invert(a, N);
   invert(b, N);
   results[3] = equals(a, b, N); // 'a' e 'b' invertido 2x.

   printf("[%i %i %i %i]\n", results[0], results[1], results[2], results[3]);
   printf("Tempo: %ld\n",wtime()-start);
   return 0;
}
