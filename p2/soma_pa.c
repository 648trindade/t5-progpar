/**
 * Programação Paralela - ELC 139
 * Profa. Andrea Schwertner Charão
 * Aluno docência Alberto Francisco Kummer Neto
 *
 * Aplicação "soma_pa"
 * Descrição: Programa que gera a soma dos n primeiros termos de uma PA.
 * Exemplo de execução:
 *
 * +-----------------------------------+
 * |   n    |    Resultado esperado    |
 * +--------+--------------------------+
 * |  100   |           5050           |
 * +--------+--------------------------+
 * |  200   |          20100           |
 * +--------+--------------------------+
 *
 */

#include <stdio.h>
#include <omp.h>
#include <sys/time.h>

int gera_termo(int index) {
   return index + 1;
}

long wtime()
{
   struct timeval t;
   gettimeofday(&t, NULL);
   return t.tv_sec*1000000 + t.tv_usec;
}

int main() {
   int soma = 0, aux = 0;
   long start = wtime();
   int repeticoes = 100000000;
   int i;

   #pragma omp parallel shared(soma, repeticoes) private(i,aux)
   {
       #pragma omp for
       for (i = 0; i < repeticoes; ++i) {
          aux += gera_termo(i);
       }
       #pragma omp atomic
       soma += aux;
   }

   printf("Soma dos %i termos da PA: %i.\n", repeticoes, soma);
   printf("%ld\n",wtime()-start);
   return 0;
}
