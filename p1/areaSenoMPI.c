/* Autor: Rafael Gauna Trindade
*  ** Integral da função sen(x) + c **
*  Limites direito e esquerdo de x e limite inferior
*  de y devem ser passados por parâmetro;
*  Tamanho de porções em 1 deve ser passado por parâmetro;
*/

#include <stdio.h>
#include <math.h>
#include <mpi/mpi.h>
#include <sys/time.h>

long wtime(){
   struct timeval t;
   gettimeofday(&t, NULL);
   return t.tv_sec*1000000 + t.tv_usec;
}

double integra(int ini, int fim, int acr, int frac, int lim){
    int i;
    double y0,y1,res=0.0,sum;
    for (i=ini; i<fim; i++){
        y0 = sin((i+0.0)/frac)+acr;
        if (y0<lim)   y0 = 0.0;
        y1 = sin((i+1.0)/frac)+acr;
        if (y1<lim)   y1 = 0.0;
        sum = (y0+y1-lim*2)/(2.0*frac);
        if (sum>0)  res += sum;
    }
    return res;
}

int main(int argc, char *argv[]){
    if (argc==6){
        int rank, size;
        MPI_Init(&argc, &argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        int range = (int)(atoi(argv[2])*atoi(argv[4]))-atoi(argv[1]);
        int bloco = (int)(range/size);
        int fim,ini = atoi(argv[1])+ (int)(rank*bloco);
        if (rank==size-1)   fim = (int)(atoi(argv[2])*atoi(argv[4]));
        else    fim = ini + bloco;
        long t = wtime();
        double rec,res = integra(ini,fim,atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
        MPI_Reduce(&res,&rec,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        if (rank==0){
            printf("Área: %.5f\n",rec);
            printf("Tempo: %ld usec\n",wtime()-t);
        }
        MPI_Finalize();
        return 0;
    }
    printf("   Implementa a integral entre f(x) = sen(x)+a e g(x) = b com limites\n   horizontais definidos.\n");
    printf(" Uso: %s <x0> <x1> <acr> <ams> <lim>\n",argv[0]);
    printf("   x0:   Limite vertical esquerdo\n");
    printf("   x1:   Limite vertical direito\n");
    printf("   acr:  Acréscimo em y para a função seno\n");
    printf("   ams:  Quantia de amostras a cada inteiro (1/ams)\n");
    printf("   lim:  Limite horizontal inferior [g(x) = lim]\n");
    return 1;
}
